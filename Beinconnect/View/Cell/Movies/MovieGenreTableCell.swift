//
//  MovieGenreTableCell.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit

private let movieDetailCollectionCellIdentifier = "MovieDetailCollectionCell"

protocol MovieGenreTableCellDelegate: class {
    func didTappedHeaderView(results:[MovieResultModel], genre:String)
    func didTappedMovie(results:MovieResultModel)
}

class MovieGenreTableCell: UITableViewCell {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerTextLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var movieResults:[MovieResultModel] = []
    
    var flowLayout: UICollectionViewFlowLayout {
        return collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
    }
    
    weak var delegate:MovieGenreTableCellDelegate?
    
    var genreName = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
        collectionViewHeight.constant = (DataManager.screenWidth/3.4 * 1.5) + 30
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedHeaderView))
        headerView.addGestureRecognizer(tapGesture)
        // Initialization code
    }
    
    func configure() {
        flowLayout.itemSize = CGSize(width: DataManager.screenWidth/3.4, height: (DataManager.screenWidth/3.4 * 1.5) + 30)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 10)
        collectionView.register(UINib(nibName: movieDetailCollectionCellIdentifier, bundle: nil), forCellWithReuseIdentifier: movieDetailCollectionCellIdentifier)
    }
    
    func setGenreLists(genre: MovieGenreDetailModel) {
        headerTextLabel.text = genre.name
        genreName = genre.name ?? ""
        if let genreId = genre.id {
            getMovieLists(genreId: genreId)
        }
    }
    
    func getMovieLists(genreId:Int) {
        Network.getMoviesByGenre(genreId: genreId) { [weak self] response in
            guard let results = response?.results else { return }
            
            self?.movieResults = results
            
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
    }
    
    @objc func tappedHeaderView() {
        delegate?.didTappedHeaderView(results: movieResults, genre: genreName)
    }
}

extension MovieGenreTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: movieDetailCollectionCellIdentifier, for: indexPath) as? MovieDetailCollectionCell {
            cell.backgroundColor = Color.clear.value
            cell.setMovieLists(genre: movieResults[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTappedMovie(results: movieResults[indexPath.row])
    }
}
