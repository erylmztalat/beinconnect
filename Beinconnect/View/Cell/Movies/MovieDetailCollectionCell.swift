//
//  MovieDetailCollectionCell.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit
import Kingfisher

class MovieDetailCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backView.layer.cornerRadius = 2
        movieImageViewHeight.constant = DataManager.screenWidth/3.4 * 1.5 
    }

    func setMovieLists(genre: MovieResultModel) {
        let imageUrlName = Network.imageEndPoint + (genre.posterPath ?? "")
    
        movieImageView.loadImageUsingCacheWithUrlString(urlString: imageUrlName)
        
        movieTitleLabel.text = genre.title
    }
}
