//
//  MoviesVC.swift
//  Beinconnect
//
//  Created by talate on 22.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit

private let movieGenreCellIdentifier = "MovieGenreTableCell"

class MoviesVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var movieGenres:[MovieGenreDetailModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Film"
        navigationController?.navigationBar.isTranslucent = false
        tableView.register(UINib(nibName: movieGenreCellIdentifier, bundle: nil), forCellReuseIdentifier: movieGenreCellIdentifier)
        getMovieGenres()
        // Do any additional setup after loading the view.
    }
    
    func getMovieGenres() {
        Network.getMovieGenres { [weak self] response in
            guard let resp = response?.genres else { return }
            
            self?.movieGenres = resp
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
}

extension MoviesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieGenres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: movieGenreCellIdentifier, for: indexPath) as? MovieGenreTableCell {
            cell.delegate = self
            cell.setGenreLists(genre: movieGenres[indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = DataManager.screenWidth/3.4 * 1.5 + 80
        return height
    }
}

extension MoviesVC: MovieGenreTableCellDelegate {
    func didTappedMovie(results: MovieResultModel) {
        let movieDetailVC = MovieDetailVC()
        movieDetailVC.movieResult = results
        navigationController?.pushViewController(movieDetailVC, animated: true)
    }
    
    func didTappedHeaderView(results: [MovieResultModel], genre: String) {
        let moviesByGenreVC = MoviesByGenreVC()
        moviesByGenreVC.movieResults = results
        moviesByGenreVC.genreName = genre
        navigationController?.pushViewController(moviesByGenreVC, animated: true)
    }
}
