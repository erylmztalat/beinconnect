//
//  MovieDetailVC.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit

class MovieDetailVC: UIViewController {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieOriginalTitleLabel: UILabel!
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieDetailLabel: UILabel!
    
    var movieResult:MovieResultModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        addBlurEffect()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        // Do any additional setup after loading the view.
    }
    
    func setupViews() {
        let imageUrlName = Network.imageEndPoint + (movieResult?.backDropPath ?? "")
        movieImageView.loadImageUsingCacheWithUrlString(urlString: imageUrlName)
        
        let imagePosterUrlName = Network.imageEndPoint + (movieResult?.posterPath ?? "")
        moviePosterImageView.loadImageUsingCacheWithUrlString(urlString: imagePosterUrlName)
        moviePosterImageView.layer.cornerRadius = 5
        
        movieTitleLabel.text = movieResult?.title
        movieOriginalTitleLabel.text = movieResult?.originalTitle
        movieDetailLabel.text = movieResult?.overview
        movieDetailLabel.numberOfLines = 10
    }
    
    func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = movieImageView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        movieImageView.addSubview(blurEffectView)
    }
}
