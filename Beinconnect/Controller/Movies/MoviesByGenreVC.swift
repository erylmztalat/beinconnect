//
//  MoviesByGenreVC.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit

private let movieDetailCollectionCellIdentifier = "MovieDetailCollectionCell"

class MoviesByGenreVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var movieResults:[MovieResultModel] = []
    
    var flowLayout: UICollectionViewFlowLayout {
        return collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
    }
    
    var genreName = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        navigationItem.title = genreName
        // Do any additional setup after loading the view.
    }
    
    func configure() {
        flowLayout.itemSize = CGSize(width: DataManager.screenWidth/3.5, height: (DataManager.screenWidth/3.5 * 1.5) + 30)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        collectionView.register(UINib(nibName: movieDetailCollectionCellIdentifier, bundle: nil), forCellWithReuseIdentifier: movieDetailCollectionCellIdentifier)
    }
}

extension MoviesByGenreVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: movieDetailCollectionCellIdentifier, for: indexPath) as? MovieDetailCollectionCell {
            cell.backgroundColor = Color.clear.value
            cell.setMovieLists(genre: movieResults[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movieDetailVC = MovieDetailVC()
        movieDetailVC.movieResult = movieResults[indexPath.row]
        navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}
