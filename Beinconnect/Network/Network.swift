//
//  Network.swift
//  Beinconnect
//
//  Created by talate on 22.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper

class Network {
    
    static let apiEndPoint = "https://api.themoviedb.org/3/"
    
    static let imageEndPoint = "http://image.tmdb.org/t/p/w185"
    
    //Movie Page
    static let movieCategoryList = "genre/movie/list?api_key=3bb3e67969473d0cb4a48a0dd61af747&language=en-US"
    static let movieList = "discover/movie?api_key=3bb3e67969473d0cb4a48a0dd61af747&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres="
    
    
    //MARK: MOVIE GENRE LISTS
    static func getMovieGenres(completion: @escaping (MovieGenreModel?) -> Void) {
        let url = apiEndPoint + movieCategoryList
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response :  DataResponse<MovieGenreModel>) in
            
            switch response.result {
            case .success(let json):
                completion(json)
            case .failure(_):
                completion(nil)
            }
        }
    }
    
    //MARK: MOVIE DETAIL BY GENRE LISTS
    static func getMoviesByGenre(genreId:Int ,completion: @escaping (MovieModel?) -> Void) {
        let url = apiEndPoint + movieList + "\(genreId)"
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response :  DataResponse<MovieModel>) in
            
            switch response.result {
            case .success(let json):
                completion(json)
            case .failure(_):
                completion(nil)
            }
        }
    }
   
}

