//
//  Color.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit

enum Color {
    
    case white
    case primary
    case tableView
    case navigation_bar
    case gray
    case lightGray
    case darkGray
    case black
    case clear
    case titleBackground
    
    // 1
    case custom(hexString: String, alpha: Double)
    // 2
    func withAlpha(_ alpha: Double) -> UIColor {
        return self.value.withAlphaComponent(CGFloat(alpha))
    }
}

extension Color {
    
    var value: UIColor {
        var instanceColor = UIColor.clear
        
        switch self {
        case .white:
            instanceColor = UIColor.white
        case .clear:
            instanceColor = UIColor.clear
        case .gray:
            instanceColor = UIColor.gray
        case .lightGray:
            instanceColor = UIColor.lightGray
        case .darkGray:
            instanceColor = UIColor.darkGray
        case .black:
            instanceColor = UIColor.black
        case .custom(let hexValue, let opacity):
            instanceColor = UIColor(hexString: hexValue).withAlphaComponent(CGFloat(opacity))
        case .primary:
            instanceColor = UIColor.black
        case .tableView:
            instanceColor = UIColor.black
        case .navigation_bar:
            instanceColor = UIColor.black
        case .titleBackground:
            instanceColor = UIColor(hexString: "1b1b1b")
        }
        return instanceColor
    }
}
