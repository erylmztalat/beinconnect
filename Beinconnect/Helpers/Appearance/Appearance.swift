//
//  Appearance.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit

class Appearance {
    static func configure(hexColor:String) {
        UINavigationBar.appearance().barStyle = .black
        UINavigationBar.appearance().barTintColor = UIColor(hexString: hexColor)
        UINavigationBar.appearance().tintColor = Color.white.value
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : Color.white.value]
    }
}
