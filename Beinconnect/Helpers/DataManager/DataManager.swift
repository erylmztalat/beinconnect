//
//  DataManager.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import UIKit

class DataManager {
    
    static var screenWidth:  CGFloat {
        get {
            return UIScreen.main.bounds.width
        }
    }
    
    static var screenHeight:  CGFloat {
        get {
            return UIScreen.main.bounds.height
        }
    }
}
