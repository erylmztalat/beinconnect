//
//  MovieModel.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import ObjectMapper

class MovieModel: Mappable {
    
    var page: Int?
    var totalResults: Int?
    var totalPages: Int?
    var results: [MovieResultModel]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        page <- map["page"]
        totalResults <- map["total_results"]
        totalPages <- map["total_pages"]
        results <- map["results"]
    }
}
