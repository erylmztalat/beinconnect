//
//  MovieCategoryModel.swift
//  Beinconnect
//
//  Created by talate on 23.02.2020.
//  Copyright © 2020 talate. All rights reserved.
//

import ObjectMapper

class MovieGenreModel: Mappable {
    
    var genres: [MovieGenreDetailModel]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        genres <- map["genres"]
    }
}
